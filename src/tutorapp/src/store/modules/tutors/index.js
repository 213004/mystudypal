import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

export default {
    namespaced: true,
    state() {
        return  {
            lastFetch: null,
            tutors: [
                {
                    id: 't1',
                    handle: 'tutor.one',
                    firstName: 'Tutor',
                    lastName: 'One',
                    areas: ['backend', 'mobile'],
                    description:
                        "A description for Tutor One. Very good words. :)",
                    hourlyRate: 50,
                },
                {
                    id: 't2',
                    handle: 'tutor.two',
                    firstName: 'Tutor',
                    lastName: 'Two',
                    areas: ['frontend', 'backend'],
                    description:
                        "A description for the Tutor Two. :)",
                    hourlyRate: 40,
                },
            ]
        }
    },
    mutations,
    actions,
    getters
  };